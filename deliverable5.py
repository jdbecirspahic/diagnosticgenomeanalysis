#!/usr/bin/env python3

"""
BFV2 Theme 05 - Genomics - Sequencing Project

Template for parsing and filtering VCF data given a certain variant
allele frequency value.

Deliverable 5
-------------
Make changes to the `parse_vcf_data` function AND the `main` function,
following the instructions preceded with double '##' symbols.

    usage:
        python3 deliverable5.py vcf_file.vcf frequency out_file.vcf

    arguments:
        vcf_file.vcf: the input VCF file, output from the varscan tool
                      frequency: a number (integer) to use as filtering value
        out_file.vcf: name of the output VCF file

    output:
        a VCF file containing the complete header (comment lines) and
        only the remaining variant positions after filtering.
"""
# IMPORT
import sys

# METADATA VARIABLES
__author__ = "Marcel Kempenaar"
__status__ = "Template"
__version__ = "2019.d5.v1"



def parse_vcf_data(vcf_input_file, frequency, vcf_output_file):
    """ This function reads the input VCF file line by line, skipping the first
    n-header lines. The remaining lines are parsed to filter out variant allele
    frequencies > frequency.
    """

    ## Open the INTPUT VCF file, read the contents line-by-line
    ## Write the first ... comment-lines (header) directly to the output file

    in_file_obj = open(vcf_input_file)
    out_file_obj = open(vcf_output_file, 'w')

    for line in in_file_obj:
        if line.startswith("#"):
            out_file_obj.write(line)
        else:
            line_split = line.split()[-1]
            freq = line_split.split(':')[6]
            freq = freq.replace('%', '')
            if float(freq) > frequency:
                out_file_obj.write(line)

    in_file_obj.close()
    out_file_obj.close()

    return 0
    ## Compare the 'FREQ' field with the `frequency` value and write the line
    ## to the output file if FREQ > frequency

# MAIN
def main(args):
    """ Main function """

    ### INPUT ###
    # Try to read input arguments from the commandline.
    # *After* testing, make sure the program gives proper errors if input is missing

    ## Change the handling of input/ output to using the `argparse` library
    if len(args) > 1:
        vcf_file = args[1]
        if not vcf_file.lower().endswith('.vcf'):
            print('Warning: given vcf file does not have the correct extension')
        out_vcf = args[2]
        if not vcf_file.lower().endswith('.vcf'):
            print('Warning: given vcf output file does not have the correct extension')
        frequency = int(args[3])
20
    else:
        print('Warning, no arguments given, using default values (testing only)...')
        vcf_file = 'example.vcf'
        out_vcf = 'd5_output.vcf'
        frequency = 30

    # Process the VCF-file
    print("Writing lines with variant nucleotides to {}".format(out_vcf))
    parse_vcf_data(vcf_file, frequency, out_vcf)
    print("Completed!")
    return 0


if __name__ == "__main__":

    sys.exit(main(sys.argv))
