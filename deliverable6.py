#!/usr/bin/env python3
"""
Deliverable 6
This script contains:
- a class made for processing an ANNOVAR output file.
- code that utilizes said class

This script prints a summary of the data for each variant from the ANNOVAR output
file.
"""

__author__ = "Jelle Becirspahic & Marijke Eggink"
__version__ = ""

from operator import itemgetter


class ProcessAnnovar:
    """
    This class takes data from an ANNOVAR output file and extracts specific collumns:
    """

    def __init__(self, file):
        """
        Prepares line from ANNOVAR output file for processing
        If give_str is True give a print a formatted summary of ANNOVAR data
        :param file:
        :return:
        """
        self.file_name = file
        refseq_gene_index = 16
        refseq_func_index = 15
        dbsnp_index = 27
        eur_index = 33
        sift_index = 34
        polyphen_index = 35
        clinvar_index = 53
        chromosome = 0
        position = 6
        ref = 3
        obs = 4
        self.columns = [refseq_gene_index, refseq_func_index, dbsnp_index,
                        eur_index, sift_index, polyphen_index, clinvar_index,
                        chromosome, position, ref, obs]

    def process_annovar_line(self, variant, header):
        """ Given a list of column indices, parse the header and all data lines """
        # Define the columns of interest

        data = variant.split("\t")

        # Extract values for selected columns
        annotation_values = itemgetter(*self.columns)(data)

        # Return list of values if header is requested
        if not header:
            return annotation_values

        # Return all data as a dictionary with column: value
        return dict(
            zip(
                [field.strip() for field in header],
                [field.strip() for field in annotation_values]
            )
        )

    def parse_annovar(self):
        """
        Opens a file and applies the process_annovar_line function to each line in the file.
        :return: a list of dictionaries with
        """
        header = []
        annovar_data = []
        with open(self.file_name, 'r') as data:
            for i, variant in enumerate(data):

                # Get header if we're processing the first line
                if i == 0:
                    header = self.process_annovar_line(variant, header=False)
                    continue  # proceed to next line

                # Process the data and add to a list
                annovar_data.append(self.process_annovar_line(variant, header))

        return annovar_data

    def __str__(self):
        return str(self.parse_annovar())


print(ProcessAnnovar('example.txt'))
